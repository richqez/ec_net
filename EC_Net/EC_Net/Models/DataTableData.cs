﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC_Net.Models
{
    public class DataTableData<T>
    {
        public string sEcho { get; set; }
        public long iTotalRecords { get; set; }
        public long iTotalDisplayRecords { get; set; }
        public List<T> aaData { get; set; }

    }
}