﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC_Net.Models
{
    public class BranchModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string FacultyCode { get; set; }
        public string FacultyName { get; set; }
        public string Created { get; set; }
    }
}