﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EC_Net.Models
{
    public class DataTableParams
    {
        public string sEcho { get; set; }

        public string sSearch { get; set; }

        public int iDisplayLength { get; set; }

        public int iDisplayStart { get; set; }

        public int iColumns { get; set; }

        public int iSortingCols { get; set; }

        public string sColumns { get; set; }

    }
}
