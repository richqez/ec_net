﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EC_Net.Models
{
    public class CategoryJobModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Created { get; set; }
    }
}