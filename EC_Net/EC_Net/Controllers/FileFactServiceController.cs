﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EC_Net.Models;

namespace EC_Net.Controllers
{

    [RoutePrefix("api/FileFactService")]
    public class FileFactServiceController : ApiController
    {
        private EasycoopContext EasycoopContext = new EasycoopContext();

        [HttpPost]
        [Route("UploadFile")]
        public Guid UploadFile()
        {
            var provider = new MultipartMemoryStreamProvider();
            Request.Content.ReadAsMultipartAsync(provider);


            var fileNameParam = provider.Contents[0].Headers.ContentDisposition.Parameters
                .FirstOrDefault(p => p.Name.ToLower() == "filename");
            var fileName = (fileNameParam == null) ? "" : fileNameParam.Value.Trim('"');
            var file = provider.Contents[0].ReadAsByteArrayAsync();
            var size = file.Result.Length;
            var ext = Path.GetExtension(fileName);

            var fileFactId = Guid.NewGuid();

            EasycoopContext.FileFact.AddOrUpdate(new FileFact()
            {
                Name = Path.GetRandomFileName(),
                Id = fileFactId,
                Created = DateTime.Now,
                Data = file.Result,
                Size = size,
                ExtFile = ext,
                FileFlag = (int)FileFactFlag.Test
                
            });

            EasycoopContext.SaveChanges();


            return fileFactId;
        }



        [HttpGet]
        [Route("DowloadFile/{id}")]
        public HttpResponseMessage DowloadFile(Guid id)
        {
            HttpResponseMessage result = null;

            var fileFact = EasycoopContext.FileFact.FirstOrDefault(t => t.Id == id);


            if (fileFact == null)
            {
                result = Request.CreateResponse(HttpStatusCode.Gone);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(fileFact.Data);
                result.Content.Headers.ContentDisposition =
                    new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileFact.Name + fileFact.ExtFile
                    };

            }

            return result;

        }




    }
}
