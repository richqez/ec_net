﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using EC_Net.Models;

namespace EC_Net.Controllers
{


    [RoutePrefix("api/FacultyService")]
    public class FacultyServiceController : ApiController
    {

        private readonly EasycoopContext EasycoopContext = new EasycoopContext();


        [HttpGet]
        [Route("Find")]
        public DataTableData<FacultyModel> Find([FromUri]DataTableParams dataTableParams)
        {
            var query = EasycoopContext.Faculty.AsQueryable();

            if (!string.IsNullOrEmpty(dataTableParams.sSearch))
            {
                query = query.Where(t =>
                    t.FacultyCode.Contains(dataTableParams.sSearch) ||
                    t.FacultyCode.Contains(dataTableParams.sSearch));
            }

                
            var result = query
                .ToList()
                .Select(t => new FacultyModel()
                {
                    Code = t.FacultyCode,
                    Name = t.FacultyName,
                    Created = t.Created.ToString("d")
                    
                })
                .ToList();


            return new DataTableData<FacultyModel>()
            {
                sEcho = dataTableParams.sEcho,
                iTotalDisplayRecords = result.Count(),
                iTotalRecords =  result.Count(),
                aaData = result
            };

        }

        [AcceptVerbs("POST")]
        [HttpPost]
        [Route("Add")]
        public void Add([FromBody]FacultyModel facultyModel)
        {

            var faculty = new Faculty()
            {
                FacultyCode = facultyModel.Code,
                FacultyName = facultyModel.Name,
                Created = DateTime.Now
            };

            EasycoopContext.Faculty.Add(faculty);
            EasycoopContext.SaveChanges();

        }
        
        
        [AcceptVerbs("POST")]
        [HttpPost]
        [Route("Update")]
        public void Update([FromBody]FacultyModel facultyModel)
        {
            var faculty = EasycoopContext.Faculty.FirstOrDefault(t => t.FacultyCode == facultyModel.Code);
            faculty.FacultyName = facultyModel.Name;
            EasycoopContext.Faculty.AddOrUpdate(faculty);
            EasycoopContext.SaveChanges();

        }

        [HttpGet]
        [Route("Delete/{code}")]
        public void Delete([FromUri]string code)
        {
            var faculty = EasycoopContext.Faculty.FirstOrDefault(t => t.FacultyCode == code);
            EasycoopContext.Faculty.Remove(faculty);
            EasycoopContext.SaveChanges();

        }



    }
}
