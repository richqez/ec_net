﻿using EC_Net.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace EC_Net.Controllers
{
    [RoutePrefix("api/CategoryJobService")]
    public class CategoryJobServiceController : ApiController
    {
        private readonly EasycoopContext EasycoopContext = new EasycoopContext();

        //[HttpGet]
        //[Route("Find")]
        //public DataTableData<BranchModel> Find([FromUri]DataTableParams dataTableParams)
        //{
        //    var query = EasycoopContext.Branch.AsQueryable();

        //    if (!string.IsNullOrEmpty(dataTableParams.sSearch))
        //    {
        //        query = query.Where(t =>
        //            t.BranchCode.Contains(dataTableParams.sSearch) ||
        //            t.BranchName.Contains(dataTableParams.sSearch));
        //    }


        //    var result = query
        //        .ToList()
        //        .Select(t => new BranchModel()
        //        {
        //            FacultyName = t.Faculty.FacultyName,
        //            FacultyCode = t.FacultyCode,
        //            Code = t.BranchCode,
        //            Name = t.BranchName,
        //            Created = t.Created.ToString("d")

        //        })
        //        .ToList();


        //    return new DataTableData<BranchModel>()
        //    {
        //        sEcho = dataTableParams.sEcho,
        //        iTotalDisplayRecords = result.Count(),
        //        iTotalRecords = result.Count(),
        //        aaData = result
        //    };

        //}

        [AcceptVerbs("POST")]
        [HttpPost]
        [Route("Add")]
        public void Add([FromBody]CategoryJobModel categoryJobModel)
        {
           
            var categoryJob = new CategoryJob()
            {
                Id = Guid.NewGuid(),
                CategoryJobName = categoryJobModel.Name,
                Created = DateTime.Now
            };

            EasycoopContext.CategoryJob.Add(categoryJob);
            EasycoopContext.SaveChanges();

        }


        //[AcceptVerbs("POST")]
        //[HttpPost]
        //[Route("Update")]
        //public void Update([FromBody]BranchModel branchModel)
        //{
        //    var branch = EasycoopContext.Branch.FirstOrDefault(t => t.BranchCode == branchModel.Code);
        //    branch.BranchName = branchModel.Name;
        //    branch.FacultyCode = branchModel.FacultyCode;
        //    EasycoopContext.Branch.AddOrUpdate(branch);
        //    EasycoopContext.SaveChanges();

        //}

        //[HttpGet]
        //[Route("Delete/{code}")]
        //public void Delete([FromUri]string code)
        //{
        //    var branch = EasycoopContext.Branch.FirstOrDefault(t => t.BranchCode == code);
        //    EasycoopContext.Branch.Remove(branch);
        //    EasycoopContext.SaveChanges();

        //}
    }
}